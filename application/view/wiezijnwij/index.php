<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 17/05/2016
 * Time: 20:40
 */
?>


<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-16" style="background-color: #32C628">
        <div class="col-md-16">
            <div class="item active">
                <div class="carousel-caption text_style_1" style="position: static; ">
                    <?php echo $this->page_name->value; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="container">
    <div class="row" style="margin-top:2%;">
        <div class="col-md-16">
            <div class="col-md-6 shadow no_padding_side" >
                <img src="<?php echo Config::get('URL') . $this->img1->value; ?>" alt="Foto met de kinderen" class="img-responsive">
            </div>
            <div class="col-md-9 col-md-offset-1">
               <?php echo $this->section1->value; ?>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:2%;">
        <div class="col-md-16">
            <div class="col-md-9">
                <?php echo $this->section2->value; ?>
            </div>
            <div class="col-md-6">
                <img style="margin-top:15%; margin-left:25%;" src="<?php echo Config::get('URL') . $this->img2->value; ?>" class="img-circle img-responsive" alt="Tiemen van Engen-wanders">
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:2%;">
        <div class="col-md-16">
            <div class="col-md-9">
                <?php echo $this->section3->value; ?>
            </div>
            <div class="col-md-6">
                <img style="margin-top:15%; margin-left:25%;" src="<?php echo Config::get('URL') . $this->img3->value; ?>" class="img-circle img-responsive" alt="Tiemen van Engen-wanders">
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:2%;">
        <div class="row">
            <div class="col-md-16" style="padding-left: 8px;">
                <p class="title">
                    Onze stagaires
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <?php echo $this->stage1->value; ?>
            </div>
            <div class="col-sm-6 col-md-4">
                <?php echo $this->stage2->value; ?>
            </div>
        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 24/04/2016
 * Time: 20:00
 *
 * 
 */
$controls = false;
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="row" style="margin-right:0px; margin-left:0px; margin-top:15px;">
    <div class="col-md-16">
        <!-- 590 x 300 px -->
        <div class="col-md-5 col-md-offset-1 hidden-xs hidden-sm shadow" style="padding-left:0px; padding-right:0px;margin-right: 1%">
            <div id="carousel-1" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-1" data-slide-to="1"></li>
                    <li data-target="#carousel-1" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:300px;<?php //echo $image_height ?>">
                    <div class="item active">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image1.jpg" alt="image 1" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image1.jpg" alt="image 2" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image1.jpg" alt="image 3" class="img-responsive">
                    </div>
                </div>
                <?php if ($controls) {?>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-2" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-2" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                <?php } ?>
            </div>
        </div>
        <!-- 472 x 300 px -->
        <div class="col-md-4 hidden-xs hidden-sm shadow" style="margin-right: 1%; padding-left:0px; padding-right:0px;">
            <div id="carousel-2" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-2" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-2" data-slide-to="1"></li>
                    <li data-target="#carousel-2" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:300px;<?php //echo $image_height ?>">
                    <div class="item active">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image2.jpg" alt="image 1" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image2.jpg" alt="image 2" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image2.jpg" alt="image 3" class="img-responsive">
                    </div>
                </div>
                <?php if ($controls) {?>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-2" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-2" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <?php } ?>
            </div>
        </div>
        <!-- 590 x 300 px -->
        <div class="col-md-5 shadow" style="padding-left:0px; padding-right:0px;">
            <div id="carousel-3" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-3" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-3" data-slide-to="1"></li>
                    <li data-target="#carousel-3" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:300px;<?php //echo $image_height ?>">
                    <div class="item active">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image1.jpg" alt="image 1" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image1.jpg" alt="image 2" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="<?php echo Config::get('URL'); ?>img/frontpage_image1.jpg" alt="image 3" class="img-responsive">
                    </div>
                </div>

                <?php if ($controls) {?>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-2" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-2" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-right:0px; margin-left:0px; margin-top:2%;"><!-- Information -->
    <div class="col-md-16" >
        <div class="col-md-9 col-md-offset-3">
            <h1 style="color:rgb(211, 60, 107); font-size: 50px;" class="">Welkom in de wereld van bso Het Oerkind!</h1>
            <p style="font-size: 25px;">
                <strong>Tuinieren in de moestuin, binnen knutselen of een spelletje doen?
                    <br>Of wil je sporen zoeken en primitief koken?
                    <br>Het kan allemaal bij Het Oerkind, de bso voor Oerkinderen van nu!
                </strong>
            </p>

        </div>
        <div class="col-md-3 hidden-xs hidden-sm" style="margin-top:2%;">
            <div class="row">
                <p>Peutergroep het Oerke</p>
                <a href="http://www.oerke.nl">www.oerke.nl</a>
            </div>
            <div class="row" style="display: none">
                <!-- <div class="fb-page" data-href="https://www.facebook.com/HetOerkind/" data-tabs="timeline,messages" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/HetOerkind/"><a href="https://www.facebook.com/HetOerkind/">Het Oerkind buitenschoolse opvang</a></blockquote></div></div> -->
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-right:0px; margin-left:0px; margin-top:2%;"><!-- Information -->
    <div class="col-md-16" >
        <div class="col-md-2 col-md-offset-3">
            <a href="<?php echo Config::get('URL')?>inschrijven" class="shadow">
                <img src="<?php echo Config::get('URL')?>img/inschrijven.png" class="img-responsive shadow">
            </a>
        </div>
        <div class="col-md-3 col-md-offset-6">
            <a href="<?php echo Config::get('URL')?>#############">
                <img src="<?php echo Config::get('URL')?>img/keurmerk.png" class="img-responsive">
            </a>
        </div>
    </div>
</div>
<div class="row" style="margin-left: 0px; margin-right:0px">
    <div class="container">
        <div class="col-md-16">
            <div class="row">
                <div class="col-md-12">
                    <p class="title" style="font-size:22px;">Onze filosofie</p>
                    <p style="font-size:18px;">Als kinderen veel buiten kunnen spelen en samen plezier maken, ontwikkelen zij zich beter. Buiten zijn is gezond, leert kinderen de wereld om zich heen ontdekken, spoort ze aan zelf dingen te ondernemen en hun grenzen te verleggen. Natuurlijk in een veilige, vertrouwde omgeving<br></p>
                </div>
            </div>
            <div class="row" style="margin-top:1%">
                <div class="col-md-10">
                    <p class="title" style="font-size:22px;">Samen ontdekken, samen leren</p>
                    <p style="font-size:18px;">
                        Samen met de kinderen ontdekken we de natuur dichtbij huis en vinden we uit hoe die natuur een rol kan spelen in ons dagelijks leven.

                        In ons eten, in ons huis, in onze tuin. <br>Door te werken met <a href="<?php echo Config::get('URL'); ?>wiezijnwij">vaste medewerkers </a> en <a href="<?php echo Config::get('URL'); ?>handelwijze">kleine groepen</a> ontstaat een echt groepsgevoel.

                        Aandacht en tijd voor ieder kind staan bij ons voorop.<br>
                    </p>
                </div>
            </div>
            <div class="row" style="margin-top:1%">
                <div class="col-md-12">
                    <p class="title" style="font-size:22px;">Een mooie natuurlijke plek in Bemmel</p>
                    <p style="font-size:18px;">De bso is gevestigd in het clubgebouw van Scouting Bemmel.

                        Een mooie binnenruimte met duurzame speel en knutselmaterialen, aangrenzend is er een buitenterrein.

                        <br>
                        Kom gerust een keer langs om de sfeer te proeven! Zijn we niet binnen?

                        Geen nood, er staat altijd op de deur waar we te vinden zijn.<br></p>
                </div>
            </div>
            <div class="row" style="margin-top:1%">
                <div class="col-md-12">
                    <p class="title" style="font-size:22px;">Een dag bij de bso</p>
                    <p>
                    <div class="embed-responsive embed-responsive-4by3">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/7DziqUIhjW0"></iframe>
                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 05/05/2016
 * Time: 13:34
 */?>

<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-16" style="background-color: #32C628">
        <div class="col-md-8 col-md-offset-8">
            <div id="carousel-1" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:100px;">
                    <div class="item active">
                        <div class="carousel-caption text_style_1" style="position: static">
                            <?php echo $this->page_activiteiten_carousel1->value; ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-caption text_style_1" style="position: static">
                            <?php echo $this->page_activiteiten_carousel2->value; ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-caption text_style_1" style="position: static">
                            <?php echo $this->page_activiteiten_carousel3->value; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-16">
                <div class="row" style="margin-top:20px;">
                    <div class="col-md-6" style="margin-bottom:10px">
                        <img src="<?php echo $this->page_activiteiten_img1->value; ?>" class="img-responsive shadow" alt="Responsive image">
                        </div>
                    <div class="col-md-10">
                        <p class="title"><?php echo $this->page_activiteiten_title_1->value; ?></p>
                        <p class="tekst"><?php echo $this->page_activiteiten_content_1->value; ?></p>
                    </div>
                </div>
                <div class="row" style="margin-top:20px;">
                    <div class="col-md-6" style="margin-bottom:10px">
                        <img src="<?php echo $this->page_activiteiten_img2->value; ?>" class="img-responsive shadow" alt="Responsive image">
                        </div>
                    <div class="col-md-10">
                        <p class="title"><?php echo $this->page_activiteiten_title_2->value; ?></p>
                        <p class="tekst"><?php echo $this->page_activiteiten_content_2->value; ?></p>
                    </div>
                </div>
                <div class="row" style="margin-top:20px;">
                    <div class="col-md-6" style="margin-bottom:10px">
                        <img src="<?php echo $this->page_activiteiten_img3->value; ?>" class="img-responsive shadow" alt="Responsive image">
                        </div>
                    <div class="col-md-10">
                        <p class="title"><?php echo $this->page_activiteiten_title_3->value; ?></p>
                        <p class="tekst"><?php echo $this->page_activiteiten_content_3->value; ?></p>
                    </div>
                </div>
        </div>
    </div>
</div>


<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 05/05/2016
 * Time: 13:34
 */?>

<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-16" style="background-color: #32C628">
        <div class="col-md-8 col-md-offset-8" style="height:100px; margin-top: 0.5%; color: white; text-align: center;">
            <div class="text_style_1">
                Inschrijven
            </div>

        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-16">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Stap 1</a>
                    </li>
                    <li role="presentation">
                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Stap 2</a>
                    </li>
                    <li role="presentation">
                        <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Stap 3</a>
                    </li>
                    <li role="presentation">
                        <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Stap 4</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">{{formulier 1}}</div>
                    <div role="tabpanel" class="tab-pane" id="profile">{{formulier 2}}</div>
                    <div role="tabpanel" class="tab-pane" id="messages">{{formulier 3}}</div>
                    <div role="tabpanel" class="tab-pane" id="settings">{{formulier 4}}</div>
                </div>


                <script type="text/javascript">
                    $('#myTabs a').click(function (e) {
                        e.preventDefault()
                        $(this).tab('show')
                    });
                    </script
<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 16/05/2016
 * Time: 16:09
 */?>
<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-16" style="background-color: #32C628">
        <div class="col-md-16">
            <div id="carousel-1" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:100px;">
                    <div class="item active">
                        <div class="carousel-caption text_style_1" style="position: static">
                            <?php echo $this->page_carousel1->value; ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-caption text_style_1" style="position: static">
                            <?php echo $this->page_carousel2->value; ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-caption text_style_1" style="position: static">
                            <?php echo $this->page_carousel3->value; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row margin_top_1">
        <div class="col-md-16">
            <div class="row" style="margin-top:1%;">
                <div class="col-md-16" style="margin-top:1%;">
                    <div class="col-md-4 shadow no_padding_side">
                        <img src="<?php echo Config::get('URL') . $this->img1->value; ?>" alt="Responsive image" class="img-responsive">
                    </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="col-md-11">
                        <?php echo $this->section1->value; ?>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:1%;">
                <div class="col-md-16" style="margin-top:1%;">

                    <div class="col-md-11">
                        <?php echo $this->section2->value; ?>
                    </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="col-md-4 shadow no_padding_side">
                        <img src="<?php echo Config::get('URL') . $this->img2->value; ?>" alt="Responsive image" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:1%;">
                <div class="col-md-16" style="margin-top:1%;">
                    <div class="col-md-4 shadow no_padding_side">
                        <img src="<?php echo Config::get('URL') . $this->img3->value; ?>" alt="Responsive image" class="img-responsive">
                    </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="col-md-11">
                        <?php echo $this->section3->value; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 16/05/2016
 * Time: 16:57
 */?>

<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 16/05/2016
 * Time: 16:09
 */?>
<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-16" style="background-color: #32C628">
        <div class="col-md-16">
                    <div class="item active">
                        <div class="carousel-caption text_style_1" style="position: static">
                            <?php echo $this->page_name->value; ?>
                        </div>
                    </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row margin_top_1">
        <div class="col-md-16">
            <div class="row" style="margin-top:1%;">
                <div class="col-md-16" style="margin-top:1%;">
                    <div class="col-md-11">
                        <?php echo $this->section1->value; ?>
                    </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="col-md-4 shadow no_padding_side">
                        <img src="<?php echo Config::get('URL') . $this->img1->value; ?>" alt="Responsive image" class="img-responsive">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-16">
                    <div class="col-md-11">
                        <form class="form-horizontal" action="<?php echo Config::get('URL'); ?>contact/mail" method="post">
                            <fieldset>

                                <!-- Form Name -->
                                <legend>Neem Contact met ons op</legend>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Uw Naam</label>
                                    <div class="col-md-8">
                                        <input id="name" name="name" placeholder="Uw naam" class="form-control input-md" required="" type="text">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mail">Uw mail</label>
                                    <div class="col-md-8">
                                        <input id="mail" name="mail" placeholder="Uw mail" class="form-control input-md" required="" type="email">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="subject">Onderwerp</label>
                                    <div class="col-md-8">
                                        <input id="subject" name="subject" placeholder="Waat gaat het over?" class="form-control input-md" required="" type="text">

                                    </div>
                                </div>

                                <!-- Textarea -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="message">Uw bericht</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" id="message" name="message"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="message">verificatie</label>
                                    <div class="col-md-8">
                                        <img style="width: 100%" id="captcha" src="<?php echo Config::get('URL'); ?>login/showCaptcha" />
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input style="width:100%; margin-top:2%;" type="text" class="form-control" name="captcha" placeholder="Vul de bovenstaande tekens in" required />
                                                    <span class="input-group-btn">
                                                        <a href="#" style="display: block; font-size: 11px; margin: 5px 0 15px 0; text-align: center" onclick="document.getElementById('captcha').src = '<?php echo Config::get('URL'); ?>login/showCaptcha?' + Math.random(); return false">
                                                            <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-refresh"></span></button>
                                                        </a>
                                                    </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                        </div><!-- /.row -->
                                    </div>
                                </div>
                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="submit"></label>
                                    <div class="col-md-8">
                                        <button id="submit" name="submit" class="btn btn-default">Verzend!</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                    <div class="col-md-1 hidden-xs hidden-sm">
                    </div>
                    <div class="col-md-4 shadow no_padding_side">
                        <img src="<?php echo Config::get('URL') . $this->img2->value; ?>" alt="Responsive image" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php

?>

<!doctype html>
<html>
<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta title="Het Oerkind">
    <!-- CSS -->
    <!-- serving HEADER.PHP -->
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/style.css<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" />
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/normalize.css<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" />
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/bootstrap.css<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" />
    <!-- Decide wether or not to keep it up here!! -->
    <script src="<?php echo Config::get('URL'); ?>js/jquery.min.js<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" type="text/javascript"></script>
    <script src="<?php echo Config::get('URL'); ?>js/bootstrap.js<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" type="text/javascript"></script>
</head>
<body>
	<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-offset-2 col-md-10">
			<!-- carousel-->
		</div>
	</div>
	<div class="row">
		<div class="col-md-16">
		</div>
	</div>
	<div class="row">
		<div class="col-md-16">
			<!--menu-->
		</div>
	</div>
<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 15/05/2016
 * Time: 12:59
 */
?>
<!doctype HTML>
<html>
<head>
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/style.css?<?php //echo rand(1,5);?>" />
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/normalize.css?<?php //echo rand(1,5);?>" />
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/bootstrap.css?<?php //echo rand(1,5);?>" />
    <script src="<?php echo Config::get('URL'); ?>js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo Config::get('URL'); ?>js/bootstrap.js" type="text/javascript"></script>
</head>
<body>
<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-16" style="background-color: #32C628; color:white;">
        <div class="col-md-8 col-md-offset-4">
            <h2 id="head">Informatie Boekje</h2>
            <p>Dit informatie boekje bevat alle benodigde informatie. Mocht je toch nog vragen hebben dan kun je contact opnemen via deze pagina: <a style="color:white;"href="<?php echo Config::get('URL')?>/contact"> <strong>Contact</strong></a></p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3" style="margin-top:45px;" id="leftCol">

            <ul class="nav nav-stacked" id="sidebar">
                <?php foreach ($this->info as $key => $value) { ?>
                    <li ><a style="padding-top: 4px; padding-bottom: 4px;" href="#<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a></li>
                <?php } ?>
            </ul>

        </div>
        <div class="col-md-12 col-md-offset-1">
            <?php foreach ($this->info as $key => $value) {?>
                <div class="info_item">
                    <h3 id="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></h3>
                    <p><?php echo $value['content']; ?></p>
                </div>
                <hr>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#sidebar').affix({
            offset: {
                top: 245
            }
        });
        var $body   = $(document.body);
        var navHeight = $('.navbar').outerHeight(true) + 10;
        $body.scrollspy({
            target: '#leftCol',
            offset: navHeight
        });
    })
</script>
</body>
</html>
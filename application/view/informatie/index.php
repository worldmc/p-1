<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-16" style="background-color: #32C628">
        <div class="col-md-16">
                    <div class="item active">
                        <div class="carousel-caption text_style_1" style="position: static; ">
                            <?php echo $this->page_name->value; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row" style="margin-top:2%;">
        <div class="col-md-16">
            <div class="col-md-6 shadow no_padding_side" >
                <img src="<?php echo Config::get('URL') . $this->img1->value; ?>" alt="Responsive image" class="img-responsive">
            </div>
            <div class="col-md-9 col-md-offset-1">
                <?php echo $this->section1->value; ?>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:2%;">
        <div class="col-md-16">
            <div class="col-md-9">
               <?php echo $this->section2->value; ?>
            </div>
            <div class="col-md-6 col-md-offset-1 shadow no_padding_side">
                <img src="<?php echo Config::get('URL') . $this->img2->value; ?>" alt="Responsive image" class="img-responsive">
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:2%;">
        <div class="col-md-16">
            <div class="col-md-6 shadow no_padding_side">
                <img src="<?php echo Config::get('URL') . $this->img3->value; ?>" alt="Responsive image" class="img-responsive">
            </div>
            <div class="col-md-9 col-md-offset-1">
                <?php echo $this->section3->value; ?>
            </div>
        </div>
    </div>
</div>
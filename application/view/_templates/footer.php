    <div class="row" style="margin-top:20px; margin-left:0px; margin-right:0px;">
        <div class="col-md-8" style="text-align:right;">
            <div class="footer"> <small><a href="http://arjovelderman.eu">Velderman Webvedevelopment -- Link</a> </small></div>
        </div>
        <div class="col-md-8">
            <p>Het Oerkind </p>
        </div>
    </div>
        <script src="<?php echo Config::get('URL'); ?>js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo Config::get('URL'); ?>js/bootstrap.js" type="text/javascript"></script>
    
    <script>
        $(document).ready(function(){
            $('body').scrollspy({ target: '#navbar-example' });
            
            $('.carousel').carousel({
                keyboard: false
            });
            /**
             * This object controls the nav bar. Implement the add and remove
             * action over the elements of the nav bar that we want to change.
             *
             * @type {{flagAdd: boolean, elements: string[], add: Function, remove: Function}}
             */
            var myNavBar = {

                flagAdd: true,

                elements: [],

                init: function (elements) {
                    this.elements = elements;
                },

                add : function() {
                    if(this.flagAdd) {
                        for(var i=0; i < this.elements.length; i++) {
                            document.getElementById(this.elements[i]).className += " fixed-theme";
                        }
                        this.flagAdd = false;
                    }
                },

                remove: function() {
                    for(var i=0; i < this.elements.length; i++) {
                        document.getElementById(this.elements[i]).className =
                            document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
                    }
                    this.flagAdd = true;
                }

            };

            /**
             * Init the object. Pass the object the array of elements
             * that we want to change when the scroll goes down
             */
            myNavBar.init(  [
                "header",
                "header-container",
                "brand"
            ]);

            /**
             * Function that manage the direction
             * of the scroll
             */
            function offSetManager(){

                var yOffset = 0;
                var currYOffSet = window.pageYOffset;

                if(yOffset < currYOffSet) {
                    myNavBar.add();
                }
                else if(currYOffSet == yOffset){
                    myNavBar.remove();
                }

            }

            /**
             * bind to the document scroll detection
             */
            window.onscroll = function(e) {
                offSetManager();
            }

            /**
             * We have to do a first detectation of offset because the page
             * could be load with scroll down set.
             */
            offSetManager();

            $('#sidebar').affix({
                offset: {
                    top: 245
                }
            });
            var $body   = $(document.body);
            var navHeight = $('.navbar').outerHeight(true) + 10;
            $body.scrollspy({
                target: '#leftCol',
                offset: navHeight
            });
        });
    </script>
</body>
</html>
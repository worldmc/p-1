<!doctype html>
<html>
<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta title="Het Oerkind">
    <!-- CSS -->
    <!-- serving HEADER.PHP -->
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/style.css<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" />
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/normalize.css<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" />
    <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/bootstrap.css<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" />
    <!-- Decide wether or not to keep it up here!! -->
    <script src="<?php echo Config::get('URL'); ?>js/jquery.min.js<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" type="text/javascript"></script>
    <script src="<?php echo Config::get('URL'); ?>js/bootstrap.js<?php if (Config::get('AMP_STYLE_CHANGE')) { echo '?' . rand(1, 5); };?>" type="text/javascript"></script>
</head>
<body style="background-color: white; position:relative; <?php if (View::checkForActiveController($filename, 'welkom')) {?> background-image: url('<?php echo Config::get('URL')?>img/background.png'); background-size:cover;<?php };?>">

<header style="padding-bottom: 100px;">
    <div class="row" style="margin-left:0px; margin-right:0px;"> <!-- header row -->
        <div class="col-md-16"> <!-- header width -->
            <!-- Fixed navbar -->
            <nav id="header" class="navbar navbar-fixed-top">
                <div id="header-container" class="container navbar-container">
                    <div class="row">
                        <div class="col-md-16">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="navbar-header">
                                        <div class="row">
                                            <div class="col-sm-2 hidden-md hidden-lg">
                                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header_nav_toggle2" aria-expanded="false" aria-controls="#header_nav_toggle2">
                                                    <span style="color:white;"class="glyphicon glyphicon-th"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                            </div>
                                            <div class="col-sm-12" style="width:100%">
                                                <a style="" class="navbar-brand" href="<?php echo Config::get('URL'); ?>"><img style=" height: inherit;" class="img-responsive" alt="Responsive image" src="<?php echo Config::get('URL'); ?>img/logo.png"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 hidden-xs hidden-sm pull-right" >
                                    <div id="header_nav_toggle" class="navbar-collapse collapse">
                                        <ul class="nav navbar-nav pull-right" style="text-align: center; margin-top:1%; margin-bottom:1%; background-color:rgba(255,255,255,0.6); font-size:18px;">
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'welkom')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>welkom">Welkom</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'activiteiten')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>activiteiten">Activiteiten</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'handelwijze')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>handelwijze">Handelwijze</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'informatie')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>informatie">Informatie</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'wiezijnwij')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>wiezijnwij">Wie zijn wij?</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'contact')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>contact">Contact</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'inschrijven')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>inschrijven">Inschrijven</a></li>
                                            <li><a href="<?php echo Config::get('URL'); ?>login/logout">Logout</a></li>
                                        </ul>
                                    </div><!-- /.nav-collapse -->
                                </div>
                                <div class="hidden-md hidden-lg col-xs-16 col-sm-16">
                                    <div id="header_nav_toggle2" class="navbar-collapse collapse">
                                        <ul class="nav navbar-nav" style="text-align: center;">
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'welkom')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>welkom">Welkom</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'activiteiten')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>activiteiten">Activiteiten</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'handelwijze')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>handelwijze">Handelwijze</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'informatie')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>informatie">Informatie</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'wiezijnwij')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>wiezijnwij">Wie zijn wij?</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'contact')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>contact">Contact</a></li>
                                            <li><a class="<?php if (View::checkForActiveController($filename, 'inschrijven')) {echo 'active'; }?>" href="<?php echo Config::get('URL'); ?>inschrijven">Inschrijven</a></li>
                                            
                                        </ul>
                                    </div><!-- /.nav-collapse -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.container -->
            </nav><!-- /.navbar -->
        </div>
    </div>
</header>




































































    <!--
    <div class="row">
        <div class="col-md-8 col-md-offset-3" style="position:relative; z-index:10;">
            <div class="row">
                <div class="col-md-16">
                    <nav style="" class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="row" style="overflow:visible">
                                <div style="float: none;" class="col-md-6">
                                    <div style="min-height: 20%;" class="navbar-header">
                                        <a style="min-width: 100%; min-height: 100%; height: 150px;" class="navbar-brand" href="#"><img style="min-height: auto; min-width: 100%;" class="img-responsive" alt="Responsive image" src="http://localhost/oerkind/public/img/logo.png"></a>


                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                </div>

                                <div class="col-md-16 col-md-offset-4" style="overflow: none; float: none;">
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-top:3%; font-size:16px;">
                                        <ul class="nav navbar-nav navbar-right" style="background: rgba(255, 255, 255, 0.75) none repeat scroll 0% 0%">
                                            <li><a href="#">Welkom</a></li>
                                            <li><a href="#">Activiteiten</a></li>
                                            <li><a href="#">Handelwijze</a></li>
                                            <li><a href="#">Informatie</a></li>
                                            <li><a href="#">Wij zijn wij?</a></li>
                                            <li><a href="#">Contact</a></li>
                                            <li><a href="#">Inschrijven</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>

                </div>
            </div>
        </div>
        <div class="col-md-2 hidden-xs hidden-sm" style="height:500px !important; heigh:auto; z-index:-10; position:relative; margin-top:1%; background-color:lime;">
        </div>
    </div>
</div>
<div class="container" style="position:relative; z-index: 10;">

        -->
<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 28/05/2016
 * Time: 16:36
 */

Class ContactModel {
    /**
     * @param $captcha
     * @param $name
     * @param $mail
     * @param $subject
     * @param $message
     * @return bool
     */
    public static function sendMail($captcha, $name, $email, $subject, $message) {
        // perform all necessary checks
        if (!CaptchaModel::checkCaptcha($captcha)) {
            Session::add('feedback_negative', Text::get('FEEDBACK_CAPTCHA_WRONG'));
            return false;
        }
        
        $body = Config::get('EMAIL_CONTACT_CONTENT') . "\r\n" . '===========================================' . "\r\n" . 'Van: ' . $name . "\r\n" . 'Onderwerp: ' . $subject . "\r\n" . 'Bericht: ' . "\r\n" . $message;

        $mail = new Mail;
        $mail_sent = $mail->sendMail($email, Config::get('EMAIL_VERIFICATION_FROM_EMAIL'),
            Config::get('EMAIL_VERIFICATION_FROM_NAME'), Config::get('EMAIL_VERIFICATION_SUBJECT') . $name, $body
        );

        if ($mail_sent) {
            Session::add('feedback_positive', Text::get('FEEDBACK_VERIFICATION_MAIL_SENDING_SUCCESSFUL'));
            return true;
        } else {
            Session::add('feedback_negative', Text::get('FEEDBACK_VERIFICATION_MAIL_SENDING_ERROR') . $mail->getError());
            return false;
        }
    }
}
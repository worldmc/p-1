<?php
/**
 * Created by PhpStorm.
 * User: arjo
 * Date: 22/06/2016
 * Time: 20:18
 */

class TextModel
{
    public static function getText($key) {
        $db = DatabaseFactory::getFactory()->fluentPDO();
        $query = $db->from('text', $key);
        $result = $query->execute()->fetch();
        return $result;
    }
}
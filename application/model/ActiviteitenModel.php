<?php

/**
 * NoteModel
 * This is basically a simple CRUD (Create/Read/Update/Delete) demonstration.
 */
class ActiviteitenModel
{
    public static function createItem($title, $image, $content) {
        $values = array(
            'id' => str_replace(' ', '', $title),
            'img' => $image,
            'title' => $title,
            'content' => $content,
            'creator' => Session::get('user_id'),
        );
        $db = $db = DatabaseFactory::getFactory()->fluentPDO();
        $query = $db->insertInto('activiteiten', $values);
        if($query->execute()) {
            LogModel::log('success', 'A item was added', $title, Session::get('user_id'));
            return true;
        } else {
            LogModel::log('error', 'Failed to add a item', $title, Session::get('user_id'));
            return false;
        }
    }
    /**
     * get all items.
     * @return mixed
     */
    public static function getAllItems() {
        $db = DatabaseFactory::getFactory()->fluentPDO();
        $query = $db->from('activiteiten');
        $result = $query->execute()->fetchAll();
        $data = json_decode(json_encode($result), true);
        return $data;
    }
    /**
     * retrieve a single item
     * @param $item_id
     * @return mixed
     */
    public static function getItem($activiteit_id) {
        $db = DatabaseFactory::getFactory()->fluentPDO();
        $query = $db->from('activiteiten')->where('id', $activiteit_id);
        $result = $query->execute()->fetch();
        $data = json_decode(json_encode($result), true);
        return $data;
    }
    /**
     * pushEdit, pushes the edit to the database.
     * @param $item_id
     * @param $title
     * @param $comment
     * @param $reuseable
     * @param $responsible
     * @return bool
     */
    public static function pushEdit($activiteit_id, $image, $title, $content) {
        $db = DatabaseFactory::getFactory()->fluentPDO();
        $set = array(
            'id' => $activiteit_id,
            'img' => $image,
            'title' => $title,
            'content' => $content,
            'creator' => Session::get('user_id'),
        );
        $query = $db->update('activiteiten', $set, $activiteit_id);
        if($query->execute()) {
            LogModel::log('success', 'A item was updated', $title, Session::get('user_id'));
            return true;
        } else {
            LogModel::log('error', 'Failed to update a item', $title, Session::get('user_id'));
            return false;
        }
    }
    /**
     * @param $item_id
     * @param $key
     * @return bool
     */
    public static function delItem($activiteit_id, $key) {
        if($key == "del") {
            $db = DatabaseFactory::getFactory()->fluentPDO();
            $query = $db->deleteFrom('activiteiten', $activiteit_id);
            if($query->execute()) {
                LogModel::log('danger', 'A item was deleted', $activiteit_id, Session::get('user_id'));
                return true;
            } else {
                LogModel::log('error', 'Failed to delete a item', $activiteit_id, Session::get('user_id'));
                return false;
            }
        } else {
            LogModel::log('error', 'Someone tried to delete a item', $activiteit_id, Session::get('user_id'));
            return false;
        }
    }
}

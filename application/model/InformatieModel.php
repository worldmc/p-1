<?php

/**
 * NoteModel
 * This is basically a simple CRUD (Create/Read/Update/Delete) demonstration.
 */
class InformatieModel
{
    public static function createItem($name, $content) {
        $values = array(
            'id' => str_replace(' ', '', $name),
            'name' => $name,
            'content' => $content,
            'creator' => Session::get('user_id'),
        );
        $db = $db = DatabaseFactory::getFactory()->fluentPDO();
        $query = $db->insertInto('informatie', $values);
        if($query->execute()) {
            LogModel::log('success', 'A item was added', $name, Session::get('user_id'));
            return true;
        } else {
            LogModel::log('error', 'Failed to add a item', $name, Session::get('user_id'));
            return false;
        }
    }
    /**
     * get all items.
     * @return mixed
     */
    public static function getAllItems() {
        $db = DatabaseFactory::getFactory()->fluentPDO();
        $query = $db->from('informatie');
        $result = $query->execute()->fetchAll();
        $data = json_decode(json_encode($result), true);
        return $data;
    }
    /**
     * retrieve a single item
     * @param $item_id
     * @return mixed
     */
    public static function getItem($info_id) {
        $db = DatabaseFactory::getFactory()->fluentPDO();
        $query = $db->from('informatie')->where('id', $info_id);
        $result = $query->execute()->fetch();
        $data = json_decode(json_encode($result), true);
        return $data;
    }
    /**
     * pushEdit, pushes the edit to the database.
     * @param $item_id
     * @param $name
     * @param $comment
     * @param $reuseable
     * @param $responsible
     * @return bool
     */
    public static function pushEdit($info_id, $name, $content) {
        $db = DatabaseFactory::getFactory()->fluentPDO();
        $set = array(
            'id' => $info_id,
            'name' => $name,
            'content' => $content,
            'creator' => Session::get('user_id'),
        );
        $query = $db->update('informatie', $set, $info_id);
        if($query->execute()) {
            LogModel::log('success', 'A item was updated', $name, Session::get('user_id'));
            return true;
        } else {
            LogModel::log('error', 'Failed to update a item', $name, Session::get('user_id'));
            return false;
        }
    }
    /**
     * @param $item_id
     * @param $key
     * @return bool
     */
    public static function delItem($info_id, $key) {
        if($key == "del") {
            $db = DatabaseFactory::getFactory()->fluentPDO();
            $query = $db->deleteFrom('informatie', $info_id);
            if($query->execute()) {
                LogModel::log('danger', 'A item was deleted', $info_id, Session::get('user_id'));
                return true;
            } else {
                LogModel::log('error', 'Failed to delete a item', $info_id, Session::get('user_id'));
                return false;
            }
        } else {
            LogModel::log('error', 'Someone tried to delete a item', $info_id, Session::get('user_id'));
            return false;
        }
    }
}

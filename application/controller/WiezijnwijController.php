<?php

class WiezijnwijController extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handles what happens when user moves to URL/index/index - or - as this is the default controller, also
     * when user moves to /index or enter your application at base level
     */
    public function index()
    {
        $this->View->render('wiezijnwij/index', array(
            'page_name' => TextModel::getText("wiezijnwij_page_name"),
            'section1' => TextModel::getText("wiezijnwij_section1"),
            'section2' => TextModel::getText("wiezijnwij_section2"),
            'section3' => TextModel::getText("wiezijnwij_section3"),
            'img1' => TextModel::getText("wiezijnwij_img1"),
            'img2' => TextModel::getText("wiezijnwij_img2"),
            'img3' => TextModel::getText("wiezijnwij_img3"),
            'stage1' => TextModel::getText("wiezijnwij_stage1"),
            'stage2' => TextModel::getText("wiezijnwij_stage2"),
        ));
    }
}

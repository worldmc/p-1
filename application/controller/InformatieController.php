<?php

class InformatieController extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handles what happens when user moves to URL/index/index - or - as this is the default controller, also
     * when user moves to /index or enter your application at base level
     */
    public function index()
    {
        $this->View->render('informatie/index', array(
            'page_name' => TextModel::getText("informatie_page_name"),
            'section1' => TextModel::getText("informatie_section1"),
            'section2' => TextModel::getText("informatie_section2"),
            'section3' => TextModel::getText("informatie_section3"),
            'img1' => TextModel::getText("informatie_img1"),
            'img2' => TextModel::getText("informatie_img2"),
            'img3' => TextModel::getText("informatie_img3")
        ));
    }

    public function boekje()
    {
        $this->View->render('informatie/boek_data', array(
            'info' => InformatieModel::getAllItems())
        );
    }
}

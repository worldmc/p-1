<?php

class ActiviteitenController extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handles what happens when user moves to URL/index/index - or - as this is the default controller, also
     * when user moves to /index or enter your application at base level
     */
    public function index()
    {
        $this->View->render('activiteiten/index', array(
            'page_carousel1' => TextModel::getText('activiteiten_page_carousel1'),
            'page_carousel2' => TextModel::getText('activiteiten_page_carousel2'),
            'page_carousel3' => TextModel::getText('activiteiten_page_carousel3'),
        ));
    }
}

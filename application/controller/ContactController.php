<?php

class ContactController extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handles what happens when user moves to URL/index/index - or - as this is the default controller, also
     * when user moves to /index or enter your application at base level
     */
    public function index()
    {
        $this->View->render('contact/index', array(
            'page_name' => TextModel::getText("contact_page_name"),
            'section1' => TextModel::getText("contact_section1"),
            'img1' => TextModel::getText("contact_img1"),
            'img2' => TextModel::getText("contact_img2"),
        ));
    }
    
    public function mail() {
        ContactModel::sendMail(Request::post('captcha'), Request::post('name'), Request::post('mail'), Request::post('subject'), Request::post('message'));
        Redirect::to('test/index');
    }
}

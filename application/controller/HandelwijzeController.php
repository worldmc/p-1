<?php

class HandelwijzeController extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handles what happens when user moves to URL/index/index - or - as this is the default controller, also
     * when user moves to /index or enter your application at base level
     */
    public function index()
    {
        $this->View->render('handelwijze/index', array(
            'page_carousel1' => TextModel::getText("handelwijze_page_carousel1"),
            'page_carousel2' => TextModel::getText("handelwijze_page_carousel2"),
            'page_carousel3' => TextModel::getText("handelwijze_page_carousel3"),
            'section1' => TextModel::getText("handelwijze_section1"),
            'section2' => TextModel::getText("handelwijze_section2"),
            'section3' => TextModel::getText("handelwijze_section3"),
            'img1' => TextModel::getText("handelwijze_img1"),
            'img2' => TextModel::getText("handelwijze_img2"),
            'img3' => TextModel::getText("handelwijze_img3")
        ));
    }
}
